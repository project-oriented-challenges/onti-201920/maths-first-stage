Задачи по математике первого этапа ОКДНТИ 2019/20
====

Данный репозиторий содержит наборы задачи по математике для 9 и 10-11 класса. Задачи использовались
на первом отборочном этапе Олимпиады КД НТИ в формате TeX.

### Инициализация локальной копии репозитория

В репозитории используются `git submodules`, поэтому для создания рабочей копии
репозитория рекомендуется сразу инициализировать все используемые модули:

```shell
git clone --recursive https://gitlab.com/project-oriented-challenges/onti-201920/maths-first-stage.git
```
или
```shell
git clone --recursive https://gitlab.com/project-oriented-challenges/onti-201920/maths-first-stage.git
```

### Сборка PDF файла

Стартовой точкой сборки материалов является файл `assignments.tex`.

Для сборки матетриалов в PDF может использоваться два варианта:

**Первый вариант**

Может осуществляться на локальной системе и полезен для того, чтобы проверить результат
сборки PDF файла перед регистрацией изменений в git.

Для этого на системе, где будет происходить сборка, необходимо наличие системы управления 
запусками программ в контейнерах `docker` и командного интерпретатора `bash`.

Перед самой первой сборкой необходимо загрузить образ `aergus/latex` на локальную систему
командой (для Ubuntu):

```shell
sudo docker pull aergus/latex
```

После чего сборку можно выполнять с помощью скрипта `build_in_docker.sh`

```shell
sudo ./build_in_docker.sh
```

* производит запуск контейнера, в котором происходит сборка PDF. Результат доступен
  в директории `build`. Поскольку запуск контейнера происходит от пользователя `root`,
  то владелец PDF файла - привелегированный пользователь.

```shell
sudo ./build_in_docker.sh clean
```

* удаляет директорию `build`


**Второй вариант**

Предполагает использование GitLab CI/CD pipelines - в этом случае сборка PDF
стартует на серверах GitLab автоматически каждый раз, когда происходит `push`.

Последний успешно созданный PDF можно найти в разделе 'Pipelines' в правой стороне
страницы, где предлагается скачать артефакты, созданные в результате соответствующего
запущенного pipeline.

Управление сборкой происходит с помощью файла `.gitlab-ci.yml` . Очевидно, что
при создании материалов другого проектного конкурса стартовая точка будет перименована.
Тогда это нужно отразить в `.gitlab-ci.yml` - нужно поменять имя `.tex` и `pdf` файлов. 